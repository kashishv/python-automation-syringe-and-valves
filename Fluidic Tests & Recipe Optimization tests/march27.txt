p hold 'Connect syringe to First tubing and insert luer in sample. Press Enter to withdraw sample into tubing.'

s 2 hexw2 1 1 7.28 25 100 0
s 2 start 0

p hold 'Pull in Air now'

s 2 hexw2 1 1 7.28 20 100 0
s 2 start 0

p hold 'Attach leur to Pre-PCR module and press Enter to proceed with infusion into mixing chamber.'

#Infusing to Pre-PCR mixing chamber
s 2 hexw2 1 0 7.28 115 75 0
s 2 start 0

#Mixing in Pre-PCR
p loop 3

ls 2 hexw2 1 1 7.28 30 75 0
ls 2 start 0

ls 2 hexw2 1 0 7.28 30 75 0
ls 2 start 0

p loop end

#Moving to PCR position

s 2 hexw2 1 0 7.28 140 75 0
s 2 start 0

p hold 'Perform PCR and press Enter when ready to proceed.'


#Moving to first mixing position in Post-PCR 5 chamber
s 2 hexw2 1 0 7.28 140 75 0
s 2 start 0

#Starting the Hyb Mixing

p hold 'Hyb Mixing'

p loop 6
ls 2 hexw2 1 1 7.28 25 75 0
ls 2 start 0

ls 2 hexw2 1 0 7.28 25 75 0
ls 2 start 0

p loop end

p hold 'Move to Reporter position'

s 2 hexw2 1 0 7.28 50 75 0
s 2 start 0


p hold 'Reporter Mixing'
p loop 3
ls 2 hexw2 1 0 7.28 30 75 0
ls 2 start 0

ls 2 hexw2 1 1 7.28 30 75 0
ls 2 start 0

p loop end


#Moving towards detection
s 2 hexw2 1 0 7.28 130 75 0
s 2 start 0

