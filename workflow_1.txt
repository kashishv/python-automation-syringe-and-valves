#Infusing at a rate of 350 ul/min and mixing at a rate of 250 ul/min. 
#Infusing in the post PCR chamber at 250ul/min

#This workflow_1.txt file controls only one channel of the pump, Channel 1.
#Please refer to SOF-0004_SW, Chemyx Fusion 4000 Automation, Python_DOC_v0.1 to learn more about how to format this text file. 

# for adding manual pause, use: p hold 'Message to be displayed here'

# for looping through commands, start with: p loop # where '#' is the number of times to loop through
# follow this with the commands to loop through starting with 'ls' for serial commands and 'lp' for python commands (pause/delay)
# end the loop section with: p loop end

# for generic serial commands, start with: s, followed by the serial command
# Generic command: s [channel number] hexw2 [unit] [mode] [syringe diameter] [volume] [rate] [delay]
# Generic command: s [channel number] start 0
# Generic command for starting both the pumps at the same time: s start 0

# Information: [unit] = 0 (ml/min), 1(ml/hr), 2(ul/min) or 3(ul/hr)
# Information: [mode] = 0 (infusion) or 1 (withdraw)
# Information: [delay]  has a unit in seconds



#--------------------------------------------------------------------------------------------------------------------




#Adding a manual pause so that the user can take their time to insert the luer and press Enter when they are ready to proceed. 
p hold 'Insert the luer of the first tubing inside the sample. Press Enter to withdraw sample into tubing.'

#Setting parameters to withdraw 25ul at 100ul/min on channel 1
s 1 hexw2 2 1 7.28 25 100 0

#Starting channel 1. Since the parameters were set in previous command, the channel withdraws 25ul.
s 1 start 0


#Adding manual pause to remove luer from sample so air can be withdrawn in.
p hold 'Press Enter to pull air into the tubing'

#Setting parameters to withdraw 20ul at 100ul/min and then starting the channel 1 to do so.
s 1 hexw2 2 1 7.28 20 100 0
s 1 start 0


#Manaul pause
p hold 'Wipe the luer of any sample using wipes. Attach leur to Pre-PCR module. Place Module inside the vertical stand. Press Enter to proceed with infusion into mixing chamber.'

#Infusing to Pre-PCR mixing chamber
s 1 hexw2 2 0 7.28 115 350 0
s 1 start 0

#Mixing in Pre-PCR. Master Mix. Mixing is done 4 times.
p loop 4

ls 1 hexw2 2 1 7.28 30 250 0
ls 1 start 0

ls 1 hexw2 2 0 7.28 30 250 0
ls 1 start 0

p loop end

#Moving to PCR position for amplication. 

s 1 hexw2 2 0 7.28 130 350 0
s 1 start 0

p hold 'Perform PCR and press Enter when ready to proceed.'


#Moving to first mixing position in Post-PCR 5 chamber
s 1 hexw2 2 0 7.28 165 350 0
s 1 start 0

#Starting the Hyb Mixing for 15 minutes. Each withdraw and infusion is a roughtly 20 seconds, hence one mix is 40 #seconds. For 15 minutes #incubation time, we need roughly 23 mixes. 

p loop 23
ls 1 hexw2 2 1 7.28 25 250 0
ls 1 start 0

ls 1 hexw2 2 0 7.28 25 250 0
ls 1 start 0

p loop end

#Moving to the Reporter position. 

s 1 hexw2 2 0 7.28 45 250 0
s 1 start 0

#Reporter mixing. for 5 minutes. Each mix is 40 seconds. For 5 minute incubation time, we require roughly 8 mixes. 


p loop 8
ls 1 hexw2 2 0 7.28 30 250 0
ls 1 start 0

ls 1 hexw2 2 1 7.28 30 250 0
ls 1 start 0

p loop end


#Moving towards detection
s 1 hexw2 2 0 7.28 130 350 0
s 1 start 0

