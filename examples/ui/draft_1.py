from __future__ import absolute_import, division, print_function
from builtins import *  # @UnusedWildImport
import sys
sys.path.append('C:\Users\Chipcare\Desktop\Python MC')
from tkinter import IntVar
from mcculw import ul
from mcculw.enums import DigitalIODirection
from examples.ui.uiexample import UIExample
from examples.props.digital import DigitalProps
from mcculw.ul import ULError
import tkinter as tk


board_num = 0
digital_props = DigitalProps(0)

port = next(
        (port for port in digital_props.port_info
            if port.supports_output), None)


if port !=None and port.is_port_configurable:
        try:
                ul.d_config_port(
                board_num, port.type, DigitalIODirection.OUT)
        except ULError as e:
                show_ul_error(e)

ul.d_bit_out(board_num, port.type,6, 0)



"""
class ULDO02(UIExample):
    def __init__(self, master=None):
            super(ULDO02, self).__init__(master)
            master.protocol("WM_DELETE_WINDOW", self.exit)

            self.board_num = 0
            self.digital_props = DigitalProps(self.board_num)

            # Find the first port that supports output, defaulting to None
            # if one is not found.
            self.port = next(
                (port for port in self.digital_props.port_info
                 if port.supports_output), None)

            # If the port is configurable, configure it for output
            if self.port != None and self.port.is_port_configurable:
                try:
                    ul.d_config_port(
                        self.board_num, self.port.type, DigitalIODirection.OUT)
                except ULError as e:
                    self.show_ul_error(e)
    def exit(self):
        # Set the port to 0 at exit
        try:
            ul.d_out(self.board_num, self.port.type, 0)
        except ULError as e:
            self.show_ul_error(e)
        self.master.destroy()   

if __name__=="__main__":
    ULDO02(master=tk.Tk()).mainloop()
    bit_value =1
    bit_num = 5
    ul.d_bit_out(self.board_num, self.port.type, bit_num, bit_value)


""" 
