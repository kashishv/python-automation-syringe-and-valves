#importing required libraries for python environment
import sys
import csv
import math
import os.path
import time
from time import sleep

#importing required libraries for syringe pump automation
import serial 
import keyboard

#import required libraries for valve control using measurement computing
from tkinter import IntVar
from mcculw import ul
from mcculw.enums import DigitalIODirection
from examples.ui.uiexample import UIExample
from examples.props.digital import DigitalProps
from mcculw.ul import ULError
import tkinter as tk

#Configuring digital I/O pins on Measurement Computing card
board_num = 0
digital_props = DigitalProps(0)

port = next(
        (port for port in digital_props.port_info
            if port.supports_output), None)


if port !=None and port.is_port_configurable:
        try:
                ul.d_config_port(
                board_num, port.type, DigitalIODirection.OUT)
        except ULError as e:
                show_ul_error(e)

#Ensuring that all pinch valves are open before beginning the program
ul.d_bit_out(board_num, port.type,0, -1)
ul.d_bit_out(board_num, port.type,1, -1)
ul.d_bit_out(board_num, port.type,2, -1)
ul.d_bit_out(board_num, port.type,3, -1)
ul.d_bit_out(board_num, port.type,4, -1)
ul.d_bit_out(board_num, port.type,5, -1)
ul.d_bit_out(board_num, port.type,6, -1)
ul.d_bit_out(board_num, port.type,7, -1)





#Setting up the serial port
comportnumber=raw_input("Enter comport number (eg.COM3) that the syringe pump is connected to: ")
comport = serial.Serial(comportnumber,9600, timeout = 1)


#Asking for the Fluidic workflow text file
print " "
filename = raw_input("Enter the name of the .txt file with your fluidic workflow (eg. example.txt): ")

#Opening the fluidic workflow text file
f=open(filename,"r")
lines = f.readlines()


#Defining a function to skip any comments in the file
info = []
def skip_comments(f):
	for line in lines:
		if not line.strip().startswith('#'):
			yield line

#Defining a function to skip all blank lines 
def skip_blanks(f):
	for l in f:
		line=l.rstrip()
		if line:
			yield line

#Skipping comments and blank lines on the fluidic workflow text file
f_1 = skip_comments(f)
f_2 = skip_blanks(f_1)

#Reading the lines and interpreting what each starting letter/combination of letters represent
for lines in f_2:
	numbers=lines.split(' ')
	limit = len(numbers)-1


	if numbers[0]=='p' and numbers[1]=='loop':
		#for loop - end; in the end of the loop, the program executes the commands inside the loop block and loops through them any given specified number of times
		if numbers[2]=='end':
			loopcommands=len(array)
			for i in range(0, loop):
				for j in range(0, loopcommands-1):
					if array[j]=='lp':
						print ("Delay of "+array[j+1])
						sleep(int(array[j+1]))
						
					elif array[j]=='ls':
						cmd = ""
						z=0
						temp=j
						while((temp+1)!=loopcommands):
							if(array[temp+1]=='lp' or array[temp+1]=='ls'):
								break
							else:
								z=z+1
							temp=temp+1
						for x in range(1,z+1):
							cmd+=array[j+x]
							cmd+=" "
						comport.write(cmd+'\r')
						#time.sleep(0.1)
						print(comport.readlines())


						comport.write("1 pump status \r")
						#time.sleep(0.1)
						status1=comport.readlines()

						comport.write("2 pump status \r")
						#time.sleep(0.1)
						status2=comport.readlines()


						while(status1[0]!='1 pump status \r0\r\n' or status2[0]!='2 pump status \r0\r\n'):
							while True:
								time.sleep(1)
								if keyboard.is_pressed('a'):
									while True:
										print "Program is on pause. Press q to resume the workflow recipe."
										time.sleep(1)
										if(keyboard.is_pressed('q')):
											break
								else:
									break
							#print 'here'
							comport.write("1 pump status \r")
							#time.sleep(0.1)	
							status1 = comport.readlines()
							print status1

							comport.write("2 pump status \r")
							#time.sleep(0.1)
							status2 = comport.readlines()
							print status2
									
		#for loop - beginning; recording the number of times the commands inside the loop will be looped through and initialing the array to store all commands inside the loop block
		else:
			loop=int(numbers[2])
			array=[]

	#for loop  -storing the commands in the right sequential order inside the loop array
	elif numbers[0]=='lp':
		for i in range(len(numbers)):
			array.append(numbers[i])
	#for loop - storing the commands in the right sequential order inside the loop array
	elif numbers[0]=='ls':
		for i in range(len(numbers)):
			array.append(numbers[i])

	#Running a syringe pump serial command
	elif numbers[0]=='s':
		cmd=""
		for x in range (limit):
			cmd += numbers[x+1]
			cmd += " "
		comport.write(cmd+'\r')
		print(comport.readlines())

		comport.write("1 pump status \r")
		status1 = comport.readlines()
		

		comport.write("2 pump status \r")
		status2 = comport.readlines()
		


		while(status1[0]!="1 pump status \r0\r\n" or status2[0]!="2 pump status \r0\r\n"):
			while True:
				time.sleep(1)
				if keyboard.is_pressed('a'):
					while True:
						print "Program is on pause. Press q to resume the workflow recipe."
						time.sleep(1)
						if(keyboard.is_pressed('q')):
							break
				else:
					break			

		
			comport.write("1 pump status \r")			
			status1 = comport.readlines()
			print status1

			comport.write("2 pump status \r")			
			status2 = comport.readlines()
			print status2
			
				
	#Python command: adding a manual pause in the program. 
	# The user can perform any other tasks during the pause.
	# The user can play around with the front panel of the pump without affecting the workflow text file.
	#The message displayed is the unique message specified by the user in the recipe.	
	# The program will not proceed in the fluidic workflow text file, until the user pressed Enter. 
	elif numbers[0]=='p' and numbers[1]=='hold':
		message=""
		for y in range (limit-1):
			message += numbers[y+2]
			message+= " "
		dummy = raw_input(message)

	#Python command: Adding a delay in seconds in the workflow recipe.
	elif numbers[0]=='p' and numbers[1]=='delay':
		print ("Delay of "+numbers[2])
		sleep(int(numbers[2]))

	elif numbers[0]=='v':
                bit_num = int(numbers[1])
                if numbers[2]=='OPEN':
                        ul.d_bit_out(board_num, port.type, bit_num, -1)
                        #sleep(int(numbers[3]))
                        #ul.d_bit_out(board_num, port.type, bit_num, 0)
                        
                elif numbers[2]=='CLOSE':
                        ul.d_bit_out(board_num, port.type, bit_num, 0)
                        #sleep(int(numbers[3]))
                        #ul.d_bit_out(board_num, port.type, bit_num, -1)

comport.close()

