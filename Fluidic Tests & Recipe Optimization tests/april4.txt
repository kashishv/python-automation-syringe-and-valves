#Infusing at a rate of 350 ul/min and mixing at a rate of 250 ul/min. 
#Infusing in the post PCR chamber at 250ul/min

#Before starting the program, ensure that the syringe is filled with 1ml water and properly attached to the syringe. 
#Connect the syringe to the first tubing with its luer on the other side not attached to the pre-PCR module.
#The rest of the tubings can be connected properly to the modules and the detection channel.
#The module should not be placed inside the vertical stand yet.

p hold 'Insert the luer of the first tubing inside the sample. Press Enter to withdraw sample into tubing.'

s 1 hexw2 1 1 7.28 25 100 0
s 1 start 0



p hold 'Press Enter to pull air into the tubing'

s 1 hexw2 1 1 7.28 20 100 0
s 1 start 0




p hold 'Wipe the luer of any sample using wipes. Attach leur to Pre-PCR module. Place Module inside the vertical stand. Press Enter to proceed with infusion into mixing chamber.'

#Infusing to Pre-PCR mixing chamber
s 1 hexw2 1 0 7.28 115 350 0
s 1 start 0

#Mixing in Pre-PCR. Master Mix. Mixing is done 4 times.
p loop 4

ls 1 hexw2 1 1 7.28 30 250 0
ls 1 start 0

ls 1 hexw2 1 0 7.28 30 250 0
ls 1 start 0

p loop end

#Moving to PCR position for amplication. 

s 1 hexw2 1 0 7.28 130 350 0
s 1 start 0

p hold 'Perform PCR and press Enter when ready to proceed.'


#Moving to first mixing position in Post-PCR 5 chamber
s 1 hexw2 1 0 7.28 175 350 0
s 1 start 0

#Starting the Hyb Mixing for 15 minutes. Each withdraw and infusion is a roughtly 20 seconds, hence one mix is 40 #seconds. For 15 minutes incubation time, we need roughly 23 mixes. 

#p hold 'Hyb Mixing'

p loop 22
ls 1 hexw2 1 1 7.28 25 250 0
ls 1 start 0

ls 1 hexw2 1 0 7.28 25 250 0
ls 1 start 0

p loop end

#Moving to the Reporter position. 
#p hold 'Move to Reporter position'

s 1 hexw2 1 0 7.28 50 250 0
s 1 start 0

#Reporter mixing. for 5 minutes. Each mix is 40 seconds. For 5 minute incubation time, we require roughly 8 mixes. 
#p hold 'Reporter Mixing'

p loop 8
ls 1 hexw2 1 0 7.28 30 250 0
ls 1 start 0

ls 1 hexw2 1 1 7.28 30 250 0
ls 1 start 0

p loop end


#Moving towards detection
s 1 hexw2 1 0 7.28 130 350 0
s 1 start 0

